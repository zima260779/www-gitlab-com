---
layout: markdown_page
title: "Category Vision - Helm Chart Registry"
---

- TOC
{:toc}

## Helm Chart Registry

Users or organizations who deploy complex pieces of software towards Kubernetes managed environments depend on a standardized way to automate provisioning those external environments. Helm is the package manager for Kubernetes and helps users define, manage, install, upgrade, and rollback even the most complex Kubernetes application. Helm uses a package format called Charts to describe a set of Kubernetes resources

Helm charts should be easy to create, version, share and publish right within GitLab. This would provide an official and integrated method to publish charts, control access, and version control them. Additionally, this will automatically allow for repository replication for multi-site development through GitLab geo.

It is possible to make your own using GitLab CI and Pages, however, we should strive for a cleaner more fully-featured solution. For example, we made our own here: https://gitlab.com/charts/charts.gitlab.io/blob/master/.gitlab-ci.yml

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Helm%20Chart%20Registry)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

### Usecases listed

We will integrate helm charts similar to other package management solutions in GitLab such as [docker](https://gitlab.com/groups/gitlab-org/-/epics/741), [maven](https://gitlab.com/groups/gitlab-org/-/epics/437), and [NPM](https://gitlab.com/groups/gitlab-org/-/epics/186) in order to see to the following use cases:

1. Public and private repositories for Helm charts
2. Fine-grained access control
3. Verification of helm chart integrity through [gitlab-org#486](https://gitlab.com/groups/gitlab-org/-/epics/486)
4. Improved accessibility and high availability through caching and proxification due to [gitlab-org#486](https://gitlab.com/groups/gitlab-org/-/epics/486)
5. Standardized workflow to version control and publish charts making use of GitLab's other services
6. Making metadata concurrently available to make workflows more transparent

## What's Next & Why

The current maturity stage of this category is targeting [minimal](https://about.gitlab.com/handbook/product/categories/maturity/) which implicates that we are just starting out. First priority is to get down the MVC [gitlab-ce#35884](https://gitlab.com/gitlab-org/gitlab-ce/issues/35884) which targets on implementing a standardized version of the [GitLab pages example](https://gitlab.com/charts/charts.gitlab.io/blob/master/.gitlab-ci.yml) given earlier which will allow the user to publish and have a relative URL which exposes an endpoint for retrieving the `.yml`.

The upside of this approach is that it immediately caters to multiple use cases listed  ([1,2, and 5](https://gitlab.com/groups/gitlab-org/-/epics/477#usecases-listed)).

After this, we'll probably target replication and availability use cases through a combination with [gitlab-org#486](https://gitlab.com/groups/gitlab-org/-/epics/486).

## Competitive Landscape

* [Helm hub](https://hub.helm.sh/)
* [Artifactory](https://www.jfrog.com/confluence/display/RTF/Helm+Chart+Repositories)
* [Chart museum](https://chartmuseum.com/)
* [Codefresh](https://codefresh.io/features/#Helm)

Next, to Helm hub which is the official helm charts repository, it is supported by products like Artifactory from Jfrog and by Codefresh. Additionally, Chart museum offers an open sourced self-managed solution, aside from being able to code one yourself with GitLab pages, [Apache](https://medium.com/@maanadev/how-set-up-a-helm-chart-repository-using-apache-web-server-670ffe0e63c7), or by using a GH repo's [raw publishing url](https://hackernoon.com/using-a-private-github-repo-as-helm-chart-repo-https-access-95629b2af27c).

It is obvious that GitLab should join the open source offering of this capability, and improve upon it with features targetted at our EE offering. This way we compete with all but do so in an integrated manner. This will give users options such as reducing risk and more fine grained control.

## Top Customer Success/Sales Issue(s)

TODO

## Top Customer Issue(s)

TODO

## Top Internal Customer Issue(s)

TODO

## Top Vision Item(s)

TODO
